package kz.zhanbolat.group.controller;

import kz.zhanbolat.group.controller.dto.ErrorResponse;
import kz.zhanbolat.group.controller.dto.StudentRequest;
import kz.zhanbolat.group.entity.Group;
import kz.zhanbolat.group.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/group")
public class GroupController {
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    private GroupService groupService;

    @GetMapping
    public List<Group> getGroups() {
        return groupService.getGroups();
    }

    @GetMapping("/{id}")
    public Group getGroup(@PathVariable("id") Long id) {
        return groupService.getGroup(id);
    }

    @GetMapping("/{id}/students/id")
    public List<Long> getStudentsIdById(@PathVariable("id") Long id) {
        return groupService.getStudentsIdById(id);
    }

    @PostMapping
    public Group createGroup(@RequestBody Group group) {
        return groupService.createGroup(group);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteGroup(@PathVariable("id") Long id) {
        groupService.deleteGroup(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public Group updateGroup(@RequestBody Group group) {
        return groupService.updateGroup(group);
    }

    @PostMapping("/{id}/assign/student/list")
    public ResponseEntity assignStudentsList(@PathVariable("id") Long id, @RequestBody List<StudentRequest> students) {
        groupService.assignStudentsList(id, students);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/unassign/student/{studentId}")
    public ResponseEntity unassignStudent(@PathVariable("id") Long id, @PathVariable("studentId") Long studentId) {
        groupService.unassignStudent(id, studentId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/student/{studentId}")
    public Group getGroupByStudentId(@PathVariable("studentId") Long studentId) {
        return groupService.getGroupByStudentId(studentId);
    }

    @GetMapping("/responsiblePerson/{responsiblePersonId}")
    public Group getGroupByResponsiblePersonId(@PathVariable("responsiblePersonId") Long responsiblePersonId) {
        return groupService.getGroupByResponsiblePersonId(responsiblePersonId);
    }

    @GetMapping("/students/id")
    public List<Long> getStudentsId() {
        return groupService.getStudentsId();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleException(Exception e) {
        logger.error("Caught exception", e);
        return new ErrorResponse(e.getMessage());
    }
}
