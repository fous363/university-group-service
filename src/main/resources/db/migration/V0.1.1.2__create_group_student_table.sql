create table if not exists student_group_students (
    group_id bigint not null,
    student_id bigint not null,
    foreign key (group_id) references student_group(id)
);