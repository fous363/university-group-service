package kz.zhanbolat.group;

import kz.zhanbolat.group.controller.dto.StudentRequest;
import kz.zhanbolat.group.entity.Group;
import kz.zhanbolat.group.service.GroupService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static kz.zhanbolat.group.TestConstant.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
public class GroupServiceTest {
    @Autowired
    private GroupService groupService;
    @Autowired
    private Function<Long, StudentRequest> studentRequestGenerator;

    @Test
    @DisplayName("GET GROUPS CASE")
    public void givenRequest_whenGetGroups_thenReturnNotEmptyList() {
        List<Group> groups = groupService.getGroups();

        assertNotNull(groups);
        assertFalse(groups.isEmpty());
    }

    @Test
    @DisplayName("GET GROUP CASE")
    public void givenNull_whenGetGroup_thenThrowException() {
        assertThrows(Exception.class, () -> groupService.getGroup(null));
    }

    @Test
    @DisplayName("GET GROUP CASE")
    public void givenNotExistingGroupId_whenGetGroup_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> groupService.getGroup(NOT_EXISTING_ID));
    }

    @Test
    @DisplayName("GET GROUP CASE")
    public void givenGroupId_whenGetGroup_thenReturnGroup() {
        Group group = groupService.getGroup(GROUP_ID);

        assertNotNull(group);
        assertEquals(GROUP_ID, group.getId());
    }

    @Test
    @DisplayName("GET STUDENTS ID BY GROUP ID CASE")
    public void givenNull_whenGetStudentsIdById_thenThrowException() {
        assertThrows(Exception.class, () -> groupService.getStudentsIdById(null));
    }

    @Test
    @DisplayName("GET STUDENTS ID BY GROUP ID CASE")
    public void givenGroupId_whenGetStudentsIdById_thenReturnNotEmptyList() {
        List<Long> studentsId = groupService.getStudentsIdById(GROUP_WITH_STUDENT_ID);

        assertNotNull(studentsId);
        assertFalse(studentsId.isEmpty());
    }

    @Test
    @DisplayName("CREATE GROUP CASE")
    public void givenNull_whenCreateGroup_thenThrowException() {
        assertAll(() -> {
            assertThrows(Exception.class, () -> groupService.createGroup(null));
            assertThrows(Exception.class, () -> groupService.createGroup(new Group()));
        });
    }

    @Test
    @DisplayName("CREATE GROUP CASE")
    public void givenGroupWithId_whenCreateGroup_thenThrowException() {
        Group group = new Group();
        group.setId(1L);
        group.setName("test");

        assertThrows(IllegalArgumentException.class, () -> groupService.createGroup(group));
    }

    @Test
    @DisplayName("CREATE GROUP CASE")
    public void givenGroup_whenCreateGroup_thenReturnGroupWithId() {
        Group group = new Group();
        group.setName("test");

        Group createdGroup = groupService.createGroup(group);

        assertNotNull(createdGroup.getId());
        assertEquals(group.getName(), createdGroup.getName());
    }

    @Test
    @DisplayName("DELETE GROUP CASE")
    public void givenNull_whenDeleteGroup_thenThrowException() {
        assertThrows(Exception.class, () -> groupService.deleteGroup(null));
    }

    @Test
    @DisplayName("DELETE GROUP CASE")
    public void givenNotExistingGroupId_whenDeleteGroup_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> groupService.deleteGroup(NOT_EXISTING_ID));
    }

    @Test
    @DisplayName("DELETE GROUP CASE")
    public void givenGroupId_whenDeleteGroup_thenDoesNotThrowException() {
        assertDoesNotThrow(() -> groupService.deleteGroup(GROUP_TO_DELETE));
    }

    @Test
    @DisplayName("UPDATE GROUP CASE")
    public void givenNull_whenUpdateGroup_thenThrowException() {
        assertAll(() -> {
            assertThrows(Exception.class, () -> groupService.updateGroup(null));
            assertThrows(Exception.class, () -> groupService.updateGroup(new Group()));
        });
    }

    @Test
    @DisplayName("UPDATE GROUP CASE")
    public void givenNotExistingGroupId_whenUpdateGroup_thenThrowException() {
        Group group = new Group();
        group.setId(NOT_EXISTING_ID);

        assertThrows(IllegalArgumentException.class, () -> groupService.updateGroup(group));
    }

    @Test
    @DisplayName("UPDATE GROUP CASE")
    public void givenGroupId_whenUpdateGroup_thenThrowException() {
        Group group = new Group();
        group.setId(GROUP_TO_UPDATE);
        group.setName("TEST");
        group.setResponsiblePersonId(1L);

        Group updatedGroup = groupService.updateGroup(group);

        assertNotNull(updatedGroup);
        assertEquals(group.getId(), updatedGroup.getId());
        assertEquals(group.getName(), updatedGroup.getName());
        assertEquals(group.getResponsiblePersonId(), updatedGroup.getResponsiblePersonId());
    }

    @Test
    @DisplayName("ASSIGN STUDENTS TO GROUP CASE")
    public void givenNull_whenAssignStudentsList_thenThrowException() {
        assertAll(() -> {
            assertThrows(Exception.class, () -> groupService.assignStudentsList(null, null));
            assertThrows(Exception.class, () -> groupService.assignStudentsList(1L, null));
            assertThrows(Exception.class, () -> groupService.assignStudentsList(null, Collections.emptyList()));
        });
    }

    @Test
    @DisplayName("ASSIGN STUDENTS TO GROUP CASE")
    public void givenEmptyList_whenAssignStudentsList_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> groupService.assignStudentsList(1L, Collections.emptyList()));
    }

    @Test
    @DisplayName("ASSIGN STUDENTS TO GROUP CASE")
    public void givenNotExistingGroupId_whenAssignStudentsList_thenThrowException() {
        assertThrows(IllegalArgumentException.class,
                () -> groupService.assignStudentsList(NOT_EXISTING_ID, Collections.singletonList(new StudentRequest())));
    }

    @Test
    @DisplayName("ASSIGN STUDENTS TO GROUP CASE")
    public void givenAssignedToGroupStudent_whenAssignStudentsList_thenThrowException() {
        StudentRequest studentRequest = new StudentRequest();
        studentRequest.setId(1L);

        assertThrows(IllegalArgumentException.class,
                () -> groupService.assignStudentsList(GROUP_WITH_STUDENT_ID, Collections.singletonList(studentRequest)));
    }

    @Test
    @DisplayName("ASSIGN STUDENTS TO GROUP CASE")
    public void givenGroupIdAndStudents_whenAssignStudentsList_thenDoesNotThrowException() {
        List<StudentRequest> studentRequests = Arrays.asList(studentRequestGenerator.apply(1L),
                studentRequestGenerator.apply(2L));

        assertDoesNotThrow(() -> groupService.assignStudentsList(GROUP_WITHOUT_STUDENTS, studentRequests));
    }

    @Test
    @DisplayName("UNASSIGN STUDENT FROM GROUP CASE")
    public void givenNull_whenUnassignStudent_thenThrowException() {
        assertAll(() -> {
            assertThrows(Exception.class, () -> groupService.unassignStudent(null, null));
            assertThrows(Exception.class, () -> groupService.unassignStudent(1L, null));
            assertThrows(Exception.class, () -> groupService.unassignStudent(null, 1L));
        });
    }

    @Test
    @DisplayName("UNASSIGN STUDENT FROM GROUP CASE")
    public void givenNotExistingGroupId_whenUnassignStudent_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> groupService.unassignStudent(1L, NOT_EXISTING_ID));
    }

    @Test
    @DisplayName("UNASSIGN STUDENT FROM GROUP CASE")
    public void givenUnassignedStudent_whenUnassignStudent_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> groupService.unassignStudent(3L, GROUP_ID));
    }

    @Test
    @DisplayName("UNASSIGN STUDENT FROM GROUP CASE")
    public void givenAssignedStudent_whenUnassignStudent_thenDoesNotThrow() {
        assertDoesNotThrow(() -> groupService.unassignStudent(GROUP_TO_UNASSIGN, 1L));
    }

    @Test
    @DisplayName("GET GROUP BY STUDENT ID CASE")
    public void givenNull_whenGetGroupByStudentId_thenThrowException() {
        assertThrows(Exception.class, () -> groupService.getGroupByStudentId(null));
    }

    @Test
    @DisplayName("GET GROUP BY STUDENT ID CASE")
    public void givenNotExistingStudentId_whenGetGroupByStudentId_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> groupService.getGroupByStudentId(NOT_EXISTING_ID));
    }

    @Test
    @DisplayName("GET GROUP BY STUDENT ID CASE")
    public void givenStudentId_whenGetGroupByStudentId_thenReturnGroup() {
        Group group = groupService.getGroupByStudentId(4L);

        assertNotNull(group);
    }

    @Test
    @DisplayName("GET GROUP BY RESPONSIBLE PERSON ID CASE")
    public void givenNull_whenGetGroupByResponsiblePersonId_thenThrowException() {
        assertThrows(Exception.class, () -> groupService.getGroupByResponsiblePersonId(null));
    }

    @Test
    @DisplayName("GET GROUP BY RESPONSIBLE PERSON ID CASE")
    public void givenNotExistingResponsiblePersonId_whenGetGroupByResponsiblePersonId_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> groupService.getGroupByResponsiblePersonId(NOT_EXISTING_ID));
    }

    @Test
    @DisplayName("GET GROUP BY RESPONSIBLE PERSON ID CASE")
    public void givenResponsiblePersonId_whenGetGroupByResponsiblePersonId_thenReturnGroup() {
        Group group = groupService.getGroupByResponsiblePersonId(1L);

        assertNotNull(group);
    }

    @Test
    @DisplayName("GET STUDENTS ID CASE")
    public void givenRequest_whenGetStudentsId_thenReturnNotEmptyList() {
        List<Long> studentsId = groupService.getStudentsId();

        assertNotNull(studentsId);
        assertFalse(studentsId.isEmpty());
    }
}
