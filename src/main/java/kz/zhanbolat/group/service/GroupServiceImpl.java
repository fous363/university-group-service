package kz.zhanbolat.group.service;

import kz.zhanbolat.group.controller.dto.StudentRequest;
import kz.zhanbolat.group.entity.Group;
import kz.zhanbolat.group.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class GroupServiceImpl implements GroupService {
    @Autowired
    private GroupRepository groupRepository;

    @Override
    public List<Group> getGroups() {
        return (List<Group>) groupRepository.findAll();
    }

    @Override
    public Group getGroup(Long id) {
        return groupRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Group with id " + id + " does not exist"));
    }

    @Override
    public List<Long> getStudentsIdById(Long id) {
        Objects.requireNonNull(id, "Id cannot be null");

        return groupRepository.findStudentsIdByGroupId(id);
    }

    @Override
    public Group createGroup(Group group) {
        Objects.requireNonNull(group, "Group cannot be null.");
        Objects.requireNonNull(group.getName(), "Group name cannot be null.");

        if (Objects.nonNull(group.getId())) {
            throw new IllegalArgumentException("Cannot create group.");
        }

        return groupRepository.save(group);
    }

    @Override
    @Transactional
    public void deleteGroup(Long id) {
        Objects.requireNonNull(id, "Id cannot be null.");

        if (!groupRepository.existsById(id)) {
            throw new IllegalArgumentException("Group with id " + id + " does not exist");
        }

        groupRepository.unassignAllStudentGroupList(id);
        groupRepository.deleteById(id);
    }

    @Override
    public Group updateGroup(Group group) {
        Objects.requireNonNull(group, "Group cannot be null.");
        Objects.requireNonNull(group.getId(), "Group id is missing");

        if (!groupRepository.existsById(group.getId())) {
            throw new IllegalArgumentException("Group with id " + group.getId() + "does not exist");
        }

        return groupRepository.save(group);
    }

    @Override
    @Transactional
    public void assignStudentsList(Long id, List<StudentRequest> students) {
        Objects.requireNonNull(id, "Id cannot be null.");
        Objects.requireNonNull(students, "Students list cannot be null.");

        if (students.isEmpty()) {
            throw new IllegalArgumentException("Students list is empty.");
        }

        if (!groupRepository.existsById(id)) {
            throw new IllegalArgumentException("Group with id " + id + " does not exist");
        }

        students.stream()
                .filter(student -> groupRepository.isStudentAlreadyAssignedToGroup(id, student.getId()))
                .findFirst()
                .ifPresent(student -> {
                    throw new IllegalArgumentException("Student " + student.getName() + " already assigned to this group.");
                });

        students.forEach(student -> groupRepository.assignStudent(id, student.getId()));
    }

    @Override
    @Transactional
    public void unassignStudent(Long id, Long studentId) {
        Objects.requireNonNull(id, "Id cannot be null.");
        Objects.requireNonNull(studentId, "Student id cannot be null.");

        if (!groupRepository.existsById(id)) {
            throw new IllegalArgumentException("Group with id " + id + " does not exist");
        }

        if (!groupRepository.isStudentAlreadyAssignedToGroup(id, studentId)) {
            throw new IllegalArgumentException("Student is unassigned to the group.");
        }

        groupRepository.unassignStudent(id, studentId);
    }

    @Override
    public Group getGroupByStudentId(Long studentId) {
        Objects.requireNonNull(studentId, " Student id cannot be null.");

        return groupRepository.findByStudentId(studentId)
                .orElseThrow(() -> new IllegalArgumentException("Group for student id " + studentId + " doesn't exist"));
    }

    @Override
    public Group getGroupByResponsiblePersonId(Long responsiblePersonId) {
        Objects.requireNonNull(responsiblePersonId, "Responsible person id cannot be null.");

        return groupRepository
                .findByResponsiblePersonId(responsiblePersonId)
                .orElseThrow(() -> new IllegalArgumentException("Group for responsible person id " + responsiblePersonId + " doesn't exist"));
    }

    @Override
    public List<Long> getStudentsId() {
        return groupRepository.findStudentsId();
    }
}
