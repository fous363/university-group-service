package kz.zhanbolat.group.repository;

import kz.zhanbolat.group.entity.Group;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {
    @Query(value = "select student_id from student_group_students where group_id = ?1", nativeQuery = true)
    List<Long> findStudentsIdByGroupId(Long id);

    @Modifying
    @Query(value = "delete from student_group_students where group_id = ?1", nativeQuery = true)
    void unassignAllStudentGroupList(Long id);

    @Query(value = "select case when count(*) > 0 then true else false end " +
            "from student_group_students where student_id = ?2 and group_id = ?1", nativeQuery = true)
    boolean isStudentAlreadyAssignedToGroup(Long id, Long studentId);

    @Modifying
    @Query(value = "insert into student_group_students(group_id, student_id) values (?1, ?2)",
            nativeQuery = true)
    void assignStudent(Long id, Long studentId);

    @Modifying
    @Query(value = "delete from student_group_students where student_id = ?2 and group_id = ?1", nativeQuery = true)
    void unassignStudent(Long id, Long studentId);

    @Query(value = "select g.id, g.name, g.responsible_id from student_group g " +
            "where g.id in (select group_id from student_group_students where student_id =  ?1)", nativeQuery = true)
    Optional<Group> findByStudentId(Long studentId);

    Optional<Group> findByResponsiblePersonId(Long responsiblePersonId);

    @Query(value = "select student_id from student_group_students", nativeQuery = true)
    List<Long> findStudentsId();
}
