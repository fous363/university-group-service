insert into student_group(name) values ('GROUP 1');

insert into student_group(name) values ('GROUP WITH STUDENTS');
insert into student_group_students(group_id, student_id) values (2, 1);
insert into student_group_students(group_id, student_id) values (2, 4);

insert into student_group(name) values ('GROUP TO DELETE');
insert into student_group_students(group_id, student_id) values (3, 1);

insert into student_group(name) values ('GROUP TO UPDATE');
insert into student_group_students(group_id, student_id) values (4, 1);

insert into student_group(name) values ('GROUP WITHOUT STUDENTS');

insert into student_group(name) values ('GROUP WITH STUDENTS TO UNASSIGN');
insert into student_group_students(group_id, student_id) values (6, 1);

insert into student_group(name, responsible_id) values ('GROUP WITH RESPONSIBLE PERSON ID', 1);