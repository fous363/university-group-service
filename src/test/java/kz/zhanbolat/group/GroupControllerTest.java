package kz.zhanbolat.group;

import kz.zhanbolat.group.controller.dto.StudentRequest;
import kz.zhanbolat.group.entity.Group;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static kz.zhanbolat.group.TestConstant.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class GroupControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private Function<Long, StudentRequest> studentRequestGenerator;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void init() {
        objectMapper = new ObjectMapper();
    }

    @Test
    @DisplayName("GET GROUPS CASE")
    @WithMockUser(authorities = "ADMIN")
    public void givenRequest_whenGetGroups_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/group/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("GET GROUP CASE")
    public void givenNotExistingGroupId_whenGetGroup_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(get("/api/group/" + NOT_EXISTING_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("GET GROUP CASE")
    public void givenGroupId_whenGetGroup_thenReturnGroup() throws Exception {
        mockMvc.perform(get("/api/group/" + GROUP_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.name").isNotEmpty())
                .andExpect(jsonPath("$.responsiblePersonId").hasJsonPath());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("GET STUDENTS BY GROUP ID CASE")
    public void givenGroupId_whenGetStudentsIdById_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/group/" + GROUP_WITH_STUDENT_ID + "/students/id"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("CREATE GROUP CASE")
    public void givenGroupWithId_whenCreateGroup_thenReturnErrorResponse() throws Exception {
        Group group = new Group();
        group.setId(1L);
        group.setName("test");

        mockMvc.perform(post("/api/group/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(group)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("CREATE GROUP CASE")
    public void givenGroup_whenCreateGroup_thenReturnGroup() throws Exception {
        Group group = new Group();
        group.setName("test");

        mockMvc.perform(post("/api/group/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(group)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.name").value(group.getName()));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("DELETE GROUP CASE")
    public void givenNotExistingGroupId_whenDeleteGroup_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(delete("/api/group/" + NOT_EXISTING_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("DELETE GROUP CASE")
    public void givenGroupId_whenDeleteGroup_thenReturnStatusOk() throws Exception {
        mockMvc.perform(delete("/api/group/" + GROUP_TO_DELETE))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("UPDATE GROUP CASE")
    public void givenNotExistingGroupId_whenUpdateGroup_thenReturnErrorResponse() throws Exception {
        Group group = new Group();
        group.setId(NOT_EXISTING_ID);

        mockMvc.perform(put("/api/group")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(group)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("UPDATE GROUP CASE")
    public void givenGroup_whenUpdate_thenReturnGroup() throws Exception {
        Group group = new Group();
        group.setId(GROUP_TO_UPDATE);
        group.setName("TEST");
        group.setResponsiblePersonId(1L);

        mockMvc.perform(put("/api/group")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(group)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(group.getId()))
                .andExpect(jsonPath("$.name").value(group.getName()))
                .andExpect(jsonPath("$.responsiblePersonId").value(group.getResponsiblePersonId()));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("ASSIGN STUDENTS TO GROUP CASE")
    public void givenNotExistingGroupId_whenAssignStudentsList_thenReturnErrorResponse() throws Exception {
        List<StudentRequest> studentRequests = Collections.singletonList(studentRequestGenerator.apply(1L));

        mockMvc.perform(post("/api/group/" + NOT_EXISTING_ID + "/assign/student/list")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(studentRequests)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("ASSIGN STUDENTS TO GROUP CASE")
    public void givenGroupIdAndAlreadyAssignedStudentToGroup_whenAssignStudentsList_thenReturnErrorResponse() throws Exception {
        List<StudentRequest> studentRequests = Collections.singletonList(studentRequestGenerator.apply(1L));

        mockMvc.perform(post("/api/group/" + GROUP_WITH_STUDENT_ID + "/assign/student/list")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(studentRequests)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("ASSIGN STUDENTS TO GROUP CASE")
    public void givenGroupIdAndStudents_whenAssignStudentsList_thenReturnStatusOk() throws Exception {
        List<StudentRequest> studentRequests = Arrays.asList(studentRequestGenerator.apply(1L), studentRequestGenerator.apply(2L));

        mockMvc.perform(post("/api/group/" + GROUP_WITHOUT_STUDENTS + "/assign/student/list")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(studentRequests)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("UNASSIGN STUDENT FROM GROUP CASE")
    public void givenNotExistingGroupId_whenUnassignStudent_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(post("/api/group/" + NOT_EXISTING_ID + "/unassign/student/" + 1L))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("UNASSIGN STUDENT FROM GROUP CASE")
    public void givenUnassignedStudent_whenUnassignStudent_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(post("/api/group/" + GROUP_ID + "/unassign/student/" + 6L))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("UNASSIGN STUDENT FROM GROUP CASE")
    public void givenAssignedStudent_whenUnassignStudent_thenReturnStatusOk() throws Exception {
        mockMvc.perform(post("/api/group/" + GROUP_TO_UNASSIGN + "/unassign/student/" + 1L))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "STUDENT")
    @DisplayName("GET GROUP BY STUDENT ID CASE")
    public void givenNotExistingId_whenGetGroupByStudentId_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(get("/api/group/student/" + NOT_EXISTING_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "STUDENT")
    @DisplayName("GET GROUP BY STUDENT ID CASE")
    public void givenStudentId_whenGetGroupByStudentId_thenReturnGroup() throws Exception {
        mockMvc.perform(get("/api/group/student/" + 4L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.name").isNotEmpty())
                .andExpect(jsonPath("$.responsiblePersonId").hasJsonPath());
    }

    @Test
    @WithMockUser(authorities = "LECTURER")
    @DisplayName("GET GROUP BY RESPONSIBLE PERSON ID CASE")
    public void givenNotExistingId_whenGetGroupByResponsiblePersonId_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(get("/api/group/responsiblePerson/" + NOT_EXISTING_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "LECTURER")
    @DisplayName("GET GROUP BY RESPONSIBLE PERSON ID CASE")
    public void givenResponsiblePersonId_whenGetGroupByResponsiblePersonId_thenReturnGroup() throws Exception {
        mockMvc.perform(get("/api/group/responsiblePerson/" + 1L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.name").isNotEmpty())
                .andExpect(jsonPath("$.responsiblePersonId").value(1L));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("GET STUDENTS ID CASE")
    public void givenRequest_whenGetStudents_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/group/students/id"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }
}
