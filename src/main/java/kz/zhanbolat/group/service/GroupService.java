package kz.zhanbolat.group.service;

import kz.zhanbolat.group.controller.dto.StudentRequest;
import kz.zhanbolat.group.entity.Group;

import java.util.List;

public interface GroupService {
    List<Group> getGroups();

    Group getGroup(Long id);

    List<Long> getStudentsIdById(Long id);

    Group createGroup(Group group);

    void deleteGroup(Long id);

    Group updateGroup(Group group);

    void assignStudentsList(Long id, List<StudentRequest> studentsId);

    void unassignStudent(Long id, Long studentId);

    Group getGroupByStudentId(Long studentId);

    Group getGroupByResponsiblePersonId(Long responsiblePersonId);

    List<Long> getStudentsId();
}
