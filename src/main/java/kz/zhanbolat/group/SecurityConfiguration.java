package kz.zhanbolat.group;

import kz.zhanbolat.group.controller.filter.AuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthorizationFilter authorizationFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin().disable()
                .logout().disable()
                .csrf().disable()
                .authorizeRequests(authorizeRequestsCustomizer ->
                    authorizeRequestsCustomizer
                        .antMatchers(HttpMethod.GET, "/api/group/").hasAuthority("ADMIN")
                        .antMatchers(HttpMethod.GET, "/api/group/*", "/api/group/*/students/id",
                                "/api/group/students/id")
                            .hasAnyAuthority("ADMIN", "STUDENT", "LECTURER")
                        .antMatchers(HttpMethod.POST, "/api/group/",
                                "/api/group/*/assign/student/list", "/api/group/*/unassign/student/*").hasAuthority("ADMIN")
                        .antMatchers(HttpMethod.DELETE, "/api/group/*").hasAuthority("ADMIN")
                        .antMatchers(HttpMethod.PUT, "/api/group").hasAuthority("ADMIN")
                        .antMatchers(HttpMethod.GET, "/api/group/student/*").hasAuthority("STUDENT")
                        .antMatchers(HttpMethod.GET, "/api/group/responsiblePerson/*").hasAuthority("LECTURER")
                        .and()
                        .addFilterBefore(jwtAuthorizationFilterBean().getFilter(), UsernamePasswordAuthenticationFilter.class))
                .cors();
    }

    @Bean
    public FilterRegistrationBean jwtAuthorizationFilterBean() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(authorizationFilter);
        registration.addUrlPatterns("/api/group**");
        registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return registration;
    }
}
