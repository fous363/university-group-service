create table if not exists student_group (
    id bigserial primary key,
    name varchar(100) not null,
    responsible_id bigint
);
