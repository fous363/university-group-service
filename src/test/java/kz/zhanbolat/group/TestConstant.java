package kz.zhanbolat.group;

public class TestConstant {
    public static final long GROUP_ID = 1L;
    public static final long NOT_EXISTING_ID = 100L;
    public static final long GROUP_WITH_STUDENT_ID = 2L;
    public static final long GROUP_TO_DELETE = 3L;
    public static final long GROUP_TO_UPDATE = 4L;
    public static final long GROUP_WITHOUT_STUDENTS = 5L;
    public static final long GROUP_TO_UNASSIGN = 6L;
}
